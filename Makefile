TARGET=\
	   main.pdf

GENERATED_TEX= \
		  generated/authors.tex \

GENERATED_PDF_FROM_SVG= \
		  generated/figs/svg/CAP_theorem.pdf \
		  generated/figs/svg/CAP_techno.pdf \
		  generated/figs/svg/bdd_colonne.pdf \
		  generated/figs/svg/extension_sharding.pdf \
		  generated/figs/svg/reduction_sharding.pdf \
		  generated/figs/svg/consistent_hashing.pdf \
		  generated/figs/svg/exempleClefsUV.pdf \
		  generated/figs/svg/pavageCoord.pdf \
		  generated/figs/svg/exemplePartitionnement.pdf \


GENERATED_PDF_FROM_UML= \
		  generated/figs/uml/introUVs.pdf \
		  generated/figs/uml/trajet.pdf \

CHAPTERS= \
		  chapters/intro.tex \
		  chapters/keyval.tex \
		  chapters/column.tex \
		  chapters/distrib_calc.tex \
		  chapters/rgpd.tex \

GENERATED= $(GENERATED_TEX) $(GENERATED_PDF_FROM_SVG) $(GENERATED_PDF_FROM_UML)

PDFLATEX=pdflatex -shell-escape -halt-on-error

main.pdf: main.tex preamble.tex $(GENERATED) $(CHAPTERS) generated/version.tex
	$(PDFLATEX) main.tex
	bibtex main.aux
	$(PDFLATEX) main.tex
	$(PDFLATEX) main.tex



.SECONDARY: $(GENERATED)

generated/version.tex: .git/refs/heads
	mkdir -p generated
	bash scripts/get_version > generated/version.tex

generated/authors.tex: AUTHORS.md
	mkdir -p generated
	bash scripts/get_authors > generated/authors.tex

generated/figs/uml/%.svg: figs/%.pu
	mkdir -p generated/figs/uml/
	plantuml -tsvg -o $(abspath generated/figs/uml/) $<

generated/figs/uml/%.pdf: generated/figs/uml/%.svg
	inkscape --without-gui --file="$<" --export-pdf="$@" || \
	inkscape "$<" -o "$@"

generated/figs/svg/%.pdf: figs/%.svg
	mkdir -p generated/figs/svg/
	inkscape --without-gui --file="$<" --export-pdf="$@" || \
	inkscape "$<" -o "$@"



.PHONY: clean mrproper

clean:
	rm -f \
		$(TARGET:%.pdf=%.aux) \
		$(TARGET:%.pdf=%.out) \
		$(TARGET:%.pdf=%.tdo) \
		$(TARGET:%.pdf=%.toc) \
		$(TARGET:%.pdf=%.log) \
		$(TARGET:%.pdf=%.bbl) \
		$(TARGET:%.pdf=%.blg) \

	rm -rf generated/


mrproper: clean
	rm -f \
		$(TARGET) \
