\chapter{Stockage clef/valeur}

  Il s'agit de la forme la plus simple de stockage, qui est utilisée dans de
  nombreux contextes: soit directement, soit indirectement par d'autres moteurs de
  stockage. La majorité des concepts introduits ici ont une portée plus générale
  dans les systèmes de base de données.

  \section{Introduction}

    La structure de données clef/valeur est disponible dans de nombreux langages
    même si le nom est susceptible de varier selon la technologie.
    \begin{itemize}
      \item \texttt{Python} : dictionnaires
      \item \texttt{Perl} : tables de hashage
      \item \texttt{C++} : \texttt{map, unordered\_map} (STL)
      \item \texttt{LISP} : \texttt{car} et \texttt{cdr} des listes 
      \item \texttt{Scala} : \texttt{Map}
      \item \texttt{JS} : \texttt{object, Map}
    \end{itemize}


    \begin{warningBox}
      \textbf{Définition:} SK super-clef: $(k_i) \in \mathbb K, \quad \forall i,j \quad k_i=k_j \iff i=j$
      \\
      
      \noindent\textbf{Définition:} K clef\\
      Soit $\mathbb K$ un sous-espace de SK. $\mathbb K$ est clef ssi $\forall \mathbb K_1, \mathbb K_2 \text{ tq } \mathbb K_1 \times \mathbb K_2 = \mathbb K, \quad \mathbb K_1 \text{ et } \mathbb K_2$ ne sont pas SK.
      Autrement dit une clef est une super-clef minimale.
      \\

      \noindent\textbf{Définition:} La structure clef--valeur est un ensemble de couples
      $\braces{ \p{k_i, v_i} }_i \subset \mathbb K \times \mathbb V$ où $i$ est le numéro d'enregistrement, $\mathbb K$ un espace de super-clefs, $\mathbb V$ un espace.
    \end{warningBox}


    \begin{infoBox}
      Le plus souvent, les clefs sont uniques: $\forall i,j,\quad  i\neq j \Rightarrow k_i \neq k_j$
      \\
      Si ce n'est pas le cas, on peut associer à une clef le $n$-upplet de
      valeurs correspondantes. Donc faire l'hypothèse d'unicité des clefs est
      \textbf{WLOG} (sans perte de généralité -- \textit{Without Loss Of
      Generality}).
    \end{infoBox}


  \section{Exemple}

    Cet exemple se base sur l'exemple du chapitre précédent.
    Une manière de mettre en forme les données pour les stocker sous forme
    d'associations clefs/valeurs est la suivante :

    \begin{pseudocodemath}
    UV(#code, (nom, filière))
    Étudiant(#login, |$\emptyset$|)
    UVÉtudiant(#(UV, etu), |$\emptyset$|)
    \end{pseudocodemath}

  \section{Opérations}

    Avec un système clef/valeur on peut définir les 4 opérations \textbf{CRUD}
    (qui permettent de \textit{tout} faire -- y compris du
    \textit{$k$-means}): \textbf{Create}, \textbf{Read}, \textbf{Update} et \textbf{Delete}.

  \section{Technologies}

    Des exemples de technologies sont:
    \todo{virer les liens et les mettre dans le bib}
    \begin{itemize}
      \item
        \href{http://www.project-voldemort.com/voldemort/}{Voldemort} (utilisé
        par exemple par Twitter)
      \item
        \href{https://redis.io/}{Redis} (utilisé par Mastodon)
      \item
        \href{https://www.oracle.com/database/berkeley-db/db.html}{BDB} (Berkley
        DB) et \href{https://symas.com/lmdb/}{LMDB} (même API), dans la même
        idée que \texttt{sqlite}, pas besoin de serveur
    \end{itemize}

    Bilan :
    \begin{itemize}
      \item \textbf{Pour :} c'est simple (et rapide et puissant)
      \item \textbf{Contre :} c'est simple
    \end{itemize}


  \section{Distribution des données}

    La distribution des données au sein des stockages de types clefs/valeurs est
    un point crucial de nombre de systèmes distribués, et les concepts
    introduits ici ont une portée plus générale, dans d'autres types de modèles
    de données.

    \subsection{Calcul de condensat (\textit{hashage})}
      \subsubsection{Introduction}

        Technique souvent utilisée : utilisation d'un condensat (\textit{hash}) parce que les
        clés ne sont pas toujours comparables (par exemple faisant partie de
        différents ensembles). Nous allons considérer un \textit{hash} comme étant
        une fonction qui respecte $\forall i, \; h(k_i) \in \intEe0N$

        Dans l'idéal, on voudrait que \textit{la fonction de hashage soit injective}, \ie:
        \[
          \forall i,j \quad k_i \neq k_j \Rightarrow  h(k_i) \neq h(k_j)
        \]
        Dans les faits, c'est impossible (quand ça arrive, on parle de
        \textit{collision}): on a plus de $N$ éléments $k_i$ à hasher, donc
        nécessairement certains auront le même hash dans $\intEe0N$.
        \footnote{Voir le principe des tiroirs pour plus de détails.}

        \begin{infoBox}
          C'est le cas de la fonction de hachage MD5, mais aussi de SHA-1,
          utilisée dans \texttt{git}. Dans certains cas, cela ne remet pas en
          question leur utilisation --- de ce point de vue, SHA-1 continue d'être
          utilisée pour \texttt{git} car les cas de collisions sont extrêmement
          rares et difficilement constructibles.
        \end{infoBox}

        Dans un contexte de distribution de données, on demande juste que $\{h(k_i)\}_i$ 
        soit à peu près uniforme.

      \begin{warningBox}
        \textbf{Définition:} hash cryptographique
        \begin{itemize}
          \item Résistance aux collisions: il est très difficile de trouver $(x, y)$ tq $h(x)=h(y)$
          \item Résistance à la 2\textsuperscript{ème} préimage: si on a $x$, il est très difficile de trouver $y$ tq $h(x)=h(y)$
          \item Résistance à la 1\textsuperscript{ère} préimage: si on a $l$, il est très difficile de trouver $x$ tq $h(x)=l$
        \end{itemize}
      \end{warningBox}

      \subsubsection{Exemples de fonctions de condensat}

        \begin{itemize}
          \item condensat non cryptographique : \texttt{CRC} (somme de redondance
            cyclique), \texttt{md5}
          \item condensat cryptographique : \texttt{sha2}, \texttt{sha3}
        \end{itemize}

      \subsection{Utilisation des condensats}

        Cas d'usage : on a une clef $k$, on cherche la donnée correspondante, si
        elle existe. On cherche donc dans un premier temps $h(k)$. On obtient
        $A = \braces{\p{k_i, v_i} \mid h(k_i)=h(k)}_i$
        qui est un ensemble \og\emph{petit}\fg. Puis il nous reste à sélectionner
        dans cet ensemble les données qui nous intéressent:
        $\braces{\p{k_i, v_i} \in A \mid k_i=k}$.
        Si le condensat est bien choisi, si la donnée existe, $A$ est la majorité
        du temps un singleton ne contenant que le couple qui nous intéresse.

        \paragraph{Exemple:} On a l'ensemble de données suivant:
        $\braces{('a',\cdots),\, ('b',\cdots),\, ('c',\cdots),\, ('f',\cdots)}$,
        avec:
        \begin{align*}
          h(a) &= 113\\
          h(b) &= 8\\
          h(c) &= 127\\
          h(f) &= 113
        \end{align*}
        On veut stocker cet ensemble de données par $h$ trié, on obtient:
        \begin{table}[H]
          \centering
          \begin{tabular}{l|l|l}
            $h$ & $k$ & $V$ \\
            \hline
            8   & 'b' & $\cdots$ \\
            113 & 'f' & $\cdots$ \\
            113 & 'a' & $\cdots$ \\
            127 & 'c' & $\cdots$ \\
          \end{tabular}
        \end{table}
        Question: quelle valeur il y a derrière $f$ ? On commence par calculer
        $h(f)=113$, puis on obtient $A=\braces{(f,\cdots),\, (a,\cdots)}$.
        Trouver les $h$ ne revient pas à trouver les clés.
        Les opérations qui sont linéaires dans la BDD peuvent être faites mais
        avec parcimonie. Ici, pour trouver un élément, on restreint la recherche
        en un temps logarithmique (pour trouver $A$), et après on a un temps linéaire en la
        taille de $A$ (qu'on espère petite).

    \subsection{Sharding}\label{sharding}

      \begin{warningBox}
        \textbf{Sharding :} Partitionnement horizontal des données
        (\ie couples \{clé, valeur\}). Chaque machine contient une partie des
        données.
      \end{warningBox}

      \paragraph{Intérêts:}
        \begin{itemize}
          \item Volume de stockage disponible (on peut mettre des
          données à plusieurs endroits) ;
          \item On peut répliquer les données (\textit{point essentiel})
          \item On va pouvoir répartir la charge (\textit{point fondamental},
          dans l'idéal on veut une répartition uniforme de la
          charge)
        \end{itemize}


        \begin{infoBox}
          Remarque: il existe aussi du partitionnement vertical où une
          partie de la valeur est stockée sur certains serveurs et ainsi de suite.
        \end{infoBox}

      
      \paragraph{Illustration : La division entière}

        Soient un espace des clefs $\intEe0N$ (ou espace des condensats des
        clefs), $p$ nœud, $n$ objets de clefs $k_i$ et de valeur $v_i$.
        Ces couples seront stockées dans le nœud ($k_i \operatorname{mod} p$)
        $\in \intEe0p$.

      \paragraph{Exemple:}

        Avec un espace des clefs $\intEe0{100}$, on a $p=3$ serveurs :\\
        Soit les données $\braces{\p{27,\cdots},\p{31,\cdots},\p{71,\cdots},\p{18,\cdots}}$
        Chaque valeur $k$ va aller dans le nœud $k \operatorname{mod} p$.

        Ainsi, la distribution des données va être effectuée comme suit:
        \begin{itemize}
          \item 1\ier serveur, nœud 0 : $\braces{\p{27,\cdots},\p{18,\cdots}}$
          \item 2\ieme serveur, nœud 1 : $\braces{\p{31,\cdots}}$
          \item 3\ieme serveur, nœud 2 : $\braces{\p{71,\cdots}}$
        \end{itemize}
        Dans ce cas, la répartition est uniforme uniquement en espérance.

        Récupération de données : soit une clef $k$. Si elle existe, elle
        est dans le nœud $k \operatorname{mod} p$.
        
        \begin{infoBox}
          Un point essentiel est que l'on n'a pas besoin de table
          stockant la localisation des données. L'existence d'une telle table
          constituerait un \textit{SPOF}.
        \end{infoBox}
      
      \paragraph{Exemple d'implémentation:}
\begin{python}
class SimpleShardStore:
    def __init__(self, nb_nodes):
        self.nb_nodes = nb_nodes
        self.nodes = [{} for _ in range(nb_nodes)]
    
    def create(self, k, v):
        self.nodes[hash(k) % self.nb_nodes][k] = v
    
    def read(self, k):
        return self.nodes[hash(k) % self.nb_nodes][k]
    
    def update(self, k, v):
        self.create(k, v)
    
    def delete(self, k):
        del self.nodes[hash(k) % self.nb_nodes][k]
\end{python}

    \subsection{\textit{Consistent hashing}}\label{consistent-hashing}

      C'est une manière avancée de distribuer les données en utilisant leur
      condensat, de telle manière à être évolutif, et à gérer de nombreuses
      situations.

      \subsubsection{Introduction et définition}

        \begin{warningBox}
          \textbf{Principe:}\\
          Considérons $\braces{\p{k_i,v_i}}$ données à stocker. On définit la
          \textbf{fonction de condensat}:
          \[
            h\colon k \rightarrow \intEe0N
          \] 
          avec $p$ nœuds de stockage. Chaque nœud $q$ stocke un intervalle de
          l'espace des condensats de la forme $\intEe{n_q}{m_q}$ (cf
          \autoref{f:keyval:consistentHashing}).

          \textit{On peut choisir des intervalles qui se recouvrent.}
        \end{warningBox}

        \begin{figure}
          \centering
          \includegraphics[width=0.5\textwidth]{consistent_hashing}
          \caption{Illustration du Consistent-hashing}
          \label{f:keyval:consistentHashing}
        \end{figure}

      \subsubsection{Propriétés}
        Il faut s'assurer que toutes les données soient stockées, c'est le cas
        si et seulement si:
        \[
          \bigcup_q\intEe{n_q}{m_q} = \intEe0N
        \]

        On peut définir le nombre de réplications pour chaque position: pour
        la position $w \in \intEe0N$, combien a-t-on de nœuds contenant $w$,
        \ie de $q$ tel que $w \in \intEe{n_q}{m_q}$ ? 
        \\
        On note ceci $R(w) = \sum_{q} \indicatrice{w \in \intEe{n_q}{m_q}}$.
        On aura $R(w) \ge 2$ dans les zones de chevauchement/superposition
        entre deux nœuds, 0 dans les zones sans nœud, et 1 dans les zones où il
        n'y a qu'un seul nœud.

        Le nombre de réplications à l'échelle de toutes les données est donné par
        \[
          R = \min_{w\in\intEe0N} R(w) = \min_{w\in\intEe0N} \sum_{q \in
          \intEe0p} \indicatrice{\intEe{n_q}{m_q}}(w)
        \] 
        
        \textit{Remarque:}
        $\bigcup_q \intE{n_q}{m_q} = \intE0N \quad \Leftrightarrow \quad R \geq 1$

        On note que cette manière de stocker permet la re-répartition des données.
      
      \subsubsection{Exemple d'implémentation}
\begin{python}
import random

class BetterShardStore:
    def __init__(self, N):
        self.nodes = [
            [0, 10, {}],
            [5, 15, {}],
            [12, N, {}],
        ]
        self.N = N
    
    def create(self, k, v):
        h = hash(k) % self.N
        for min, max, store in self.nodes:
            if min <= h < max:
                store[k] = v
    
    def read(self, k):
        h = hash(k) % self.N
        return random.choice([store for (min, max, store) in self.nodes if min <= h < max])[k]
    
    def update(self, k, v):
        self.create(k, v)
    
    def delete(self, k):
        h = hash(k) % self.N
        for min, max, store in self.nodes:
            if min <= h < max:
                del store[k]
\end{python}

      \subsubsection{Illustration}

        \paragraph{Re-répartition des données}
       
          \todo{blabla adapté à la figure}
          \begin{figure}
            \centering
            \includegraphics[width=0.7\textwidth]{extension_sharding}
            \caption{Illustration de l'extension-sharding}
            \label{f:keyval:extensionSharding}
          \end{figure}

          Au départ, on a $R(w) = 1$ sur toute la base de données. Au fur et à
          mesure du temps, il récupère des données : certains nœuds
          \textit{abandonnent} des données au profit du nouveau nœud.

        \paragraph{Mort d'un nœud}
        
          \todo{blabla adapté à la figure}
          \begin{figure}
            \centering
            \includegraphics[width=0.7\textwidth]{reduction_sharding}
            \caption{Illustration du reduction-sharding}
            \label{f:keyval:reductionSharding}
          \end{figure}

          On considère le cas où $R=2$ (s'il vaut 1, on se retrouve dans une
          situation où la donnée n'est liée qu'à un seul nœud, donc la mort de
          ce dernier pose problème). Exemple au tableau de base où les $R(w)$
          valent 2, sauf dans une zone où il vaut 3 (cf \autoref{f:keyval:reductionSharding}). Si le nœud
          bleu (à cheval entre les zones où $R(w)$ vaut 2 et 3) meurt, on perd
          le niveau de réplication $R=2$ (vu qu'on se retrouve avec $R=1$). Dans cette
          situation, on peut faire grossir les nœuds restants pour combler les «~trous~»
          laissés où $R=1$ jusqu'à réobtenir $R=2$. Cela se traduit par le transfert
          des données contenues dans l'intervalle où $R(w) = 1$ aux nœuds
          adjacents.

      \subsection{Intérêt} 
        \begin{infoBox}
          \textbf{Absence de table de distribution:}
          Pour retrouver une donnée, on a seulement besoin de connaître les
          bornes des différents intervalles $(n_q,m_q)$.
          Pas besoin de table de stockage de distribution des données.
        \end{infoBox}

        \begin{infoBox}
          \textbf{Hantise de la panne simultanée:} Lors de la mort d'un
          disque, ses données (déjà dupliquées sur d'autres) doivent être
          copiées vers d'autres nœuds pour assurer à nouveau une duplication.
          Cela nécessite beaucoup d'accès en lecture et en écriture, ce qui
          peut augmenter les chances de pannes sur les autres disques.
        \end{infoBox}

    \subsection{Topologie et stockage (pédagogie par l'exemple)}
    \label{topologie-et-stockage-pedagogie-par-lexemple}

      \paragraph{Contexte de l'exemple:}

        On veut pouvoir interroger le cluster depuis n'importe où. Il faut ici
        une stratégie de réplication qui respecte la topologie.

        Exemple: des données sont présentes sur trois sites dans le
        monde (USA, France, Océanie) mais correspondent à la même base de
        données. On doit avoir $R\geq3$ (pour que les données soient accessibles
        sur les trois lieux en même temps).

        On veut une \textit{non colocalisation des réplications} : Les serveurs
        sont sur des sites différents pour éviter les dégâts liés à des
        évènements exceptionnels, et pour qu'une lecture soit aussi proche
        que possible de n'importe quel endroit sur Terre.

        \todo{insérer figure, et blabla adapté}
        On a $R_{global} =3$ (superposer les trois zones pour s'en rendre
        compte).

        Si le nœud bleu de $Z_1$ meurt, il faut recréer les données perdues
        dans la $Z_1$ depuis les données de $Z_2$ ou $Z_3$, il va donc
        falloir faire un transfert.

        \begin{infoBox}
          Lien avec le CAP theorem : tolérance au partitionnement ici.
        \end{infoBox}

        \begin{infoBox}
          Cassandra utilise le consistent hashing par exemple mais de 
          nombreux autres systèmes utilisent également ce principe.
          Par exemple Spark permet de définir comment répartir les données
          sur les nœuds selon plusieurs stratégies via l'API des RDD.
          \cite{sparkPartitionning}
        \end{infoBox}

